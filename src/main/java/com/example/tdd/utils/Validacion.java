package com.example.tdd.utils;

public class Validacion {
    public boolean validarSoloLetras(String nombre) {
        return nombre.matches("[a-zA-Z]+") && !nombre.contains(" ");
    }

    public boolean validarSoloNumeros(int edad) {
        return edad >= 0;
    }


    public boolean validarRut(String rut) {
        rut = rut.toUpperCase();

        if (rut.length() == 9 || rut.length() == 10) {
            String[] partes = rut.split("-");
            String rutNumerico = partes[0];
            String digitoVerificador = partes[1];

            int rutNumericoInt;
            try {
                rutNumericoInt = Integer.parseInt(rutNumerico);
            } catch (NumberFormatException e) {

                return false;
            }

            int i = 0, suma = 1;
            while (rutNumericoInt != 0) {
                suma = (suma + rutNumericoInt % 10 * (9 - i++ % 6)) % 11;
                rutNumericoInt /= 10;
            }

            char digitoCalculado = (char) (suma != 0 ? suma + 47 : 75);

            if (digitoCalculado != digitoVerificador.charAt(0)) {
                System.out.println("El RUT ingresado es inválido");
                return false;
            }

            System.out.println("El RUT ingresado es válido");
            return true;
        } else {
            System.out.println("El RUT ingresado tiene un largo incorrecto");
            return false;
        }
    }


    public boolean validarNumero(String telefono) {
        String numeroLimpiado = telefono.replaceAll("[^0-9+]", "");

        if (numeroLimpiado.matches("\\+?569\\d{8}$") || numeroLimpiado.matches("9\\d{8}$")) {
            if (telefono.matches(".*[a-zA-Z].*")) {
                System.out.println("Número de teléfono contiene letras.");
                return false;
            }
            System.out.println("Número de teléfono válido");
            return true;
        } else {
            System.out.println("Número de teléfono inválido");
            return false;
        }
    }


}