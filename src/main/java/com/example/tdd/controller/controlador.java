package com.example.tdd.controller;

import com.example.tdd.utils.Validacion;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class controlador {
    private Validacion validacion;

    public controlador() {
        this.validacion = new Validacion();
    }

    @PostMapping("/usuario")
    public String crearUsuario(@RequestBody Map<String, Object> usuario) {

        String nombre = (String) usuario.get("nombre");
        if (!validacion.validarSoloLetras(nombre)) {
            return "Nombre inválido";
        }


        String apellidoPaterno = (String) usuario.get("apellidoPaterno");

        if (!validacion.validarSoloLetras(apellidoPaterno) ) {
            return "Apellido Paterno invalido";
        }


        String apellidoMaterno = (String) usuario.get("apellidoMaterno");

        if (!validacion.validarSoloLetras(apellidoMaterno)) {
            return "Apellido Materno invalido";
        }
        String rut = (String) usuario.get("rut");
        if (!validacion.validarRut(rut)) {
            return "RUT inválido";
        }


        String numeroTelefono = (String) usuario.get("numeroTelefono");
        if (!validacion.validarNumero(numeroTelefono)) {
            return "Número telefónico inválido";
        }

        // Validar edad
        int edad = (int) usuario.get("edad");
        if (!validacion.validarSoloNumeros(edad)) {
            return "Edad inválida";
        }


        return "Usuario creado exitosamente";
    }
}
