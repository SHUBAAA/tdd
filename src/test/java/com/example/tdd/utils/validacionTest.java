package com.example.tdd.utils;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class validacionTest {

    Validacion validacion;

    @BeforeEach
    void setUp() {
        validacion = new Validacion();
    }

    @AfterEach
    void tearDown() {
    }


    @Test
    public void testNombreBueno() {
        String nombre = "Ricardo";

        assertTrue(validacion.validarSoloLetras(nombre));
    }

    @Test
    public void testNombreMalo() {
        String nombre = "Ricardo ";

        assertFalse(validacion.validarSoloLetras(nombre));
    }

    @Test
    public void testApellidoBueno() {
        String apellido = "Martinez";

        assertTrue(validacion.validarSoloLetras(apellido));
    }

    @Test
    public void testEdadBueno() {
        int edad = 1;

        assertTrue(validacion.validarSoloNumeros(edad));
    }

    @Test
    public void testEdadMala() {
        int edad = -1;

        assertFalse(validacion.validarSoloNumeros(edad));
    }

    @Test
    public void testRut() {
        String rut = "21255429-4";

        assertTrue(validacion.validarRut(rut));
    }

    @ParameterizedTest
    @ValueSource(strings = {"21255429-k", "32134213", "j"})
    public void testRutInvalido(String rut) {

        assertFalse(validacion.validarRut(rut));
    }

    @ParameterizedTest
    @ValueSource(strings = {"+56940789167", "+569 40789156","+56 940789156","940789156"})
    public void testTelefono(String telefono) {

        assertTrue(validacion.validarNumero(telefono));
    }

    @ParameterizedTest
    @ValueSource(strings = {"+569407891", "56+9 409156","+56 9407ffgd89156","9fsdfsd","+969 40789156"})
    public void testTelefonoMalos(String telefono) {

        assertFalse(validacion.validarNumero(telefono));
    }
}
